#include"DatosMemComp.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>



int main(){
	int fd;
	DatosMemComp *pdatosComp;
	
	//Abrimos fichero
	fd = open("datosComp",O_RDWR);
	
	//Mensajes de error
	if(fd < 0) 
	{
		perror("Error al abrir el fichero");
		exit(1);
	}
	if((pdatosComp = static_cast<DatosMemComp*>(mmap(0,sizeof(DatosMemComp),PROT_READ|PROT_WRITE,MAP_SHARED,fd,0)))==MAP_FAILED)
	{
		perror("Error en la proyeccion del fichero");
		close(fd);
		return 1;
	}
	close(fd);
	
	while(1)
	{
		if((pdatosComp->esfera.centro.y) < (pdatosComp->raqueta1.getCentro().y))
		{
			pdatosComp -> accion = -1;
		}
		
		if((pdatosComp->esfera.centro.y) == (pdatosComp->raqueta1.getCentro().y))
		{
			pdatosComp -> accion = 0;
		}
		if((pdatosComp->esfera.centro.y) > (pdatosComp->raqueta1.getCentro().y))
		{
			pdatosComp -> accion = 1;
		}
		usleep(25000);
	}
	
	munmap(pdatosComp, sizeof(DatosMemComp));
	unlink("datosComp");
	return 1;
}
