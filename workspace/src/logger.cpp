#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include "Puntuacion.h"

int main(){
	
	int fd;
	Puntuacion puntos;
	
	//creamos el FIFO
	if(mkfifo("FIFO",0600) < 0)
	{
	//Mensaje de error
		perror("Error al crear la tuberia");
		return 1;
	}
	
	//Abrimos la tuberia con permiso de lectura 
	if((fd=open("FIFO",O_RDONLY)) < 0)
	{
	//Mensaje de error
		perror("No se ha abierto FIFO");
		return 1;
	}
	
	//Puntuacion de los jugadores
	while ((read(fd,&puntos,sizeof(puntos))) == sizeof(puntos))
	{
		if(puntos.lastWinner == 1)
		{
			printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador1);
		}
		if(puntos.lastWinner == 2)
		{
			printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador2);
		}
	}
	//Cerramos la tuberia
	close(fd);
	unlink("FIFO");
	return 0;

}
